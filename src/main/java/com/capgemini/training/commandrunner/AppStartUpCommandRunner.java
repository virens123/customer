package com.capgemini.training.commandrunner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.capgemini.training.customer.entity.Address;
import com.capgemini.training.customer.entity.Communication;
import com.capgemini.training.customer.entity.Customer;
import com.capgemini.training.service.AddressService;
import com.capgemini.training.service.CommunicationService;
import com.capgemini.training.service.CustomerService;

@Component
public class AppStartUpCommandRunner implements CommandLineRunner {

	@Autowired
	CustomerService customerService;
	@Autowired
	AddressService addressService;
	
	@Autowired
	CommunicationService communicationService;
	
	
	@Override
	public void run(String... args) throws Exception {
	
		Address a1=new Address();
		a1.setCity("Mumbai");
		a1.setCompany("Capgemini");
		a1.setCountryId("Ind");
		a1.setCustomerAddressLabel("CapgCorp");
		//a1.setEntityId(11L);
		a1.setFax("faxNo");
		a1.setFirstName("Pooja");
		a1.setIsDefaultBilling("Billing");
		a1.setIsDefaultShipping("Shiiping");
		a1.setLastName("jain");
		a1.setMissingVerification("missing");
		a1.setPickpointTerminalId("terminalId");
		a1.setPickpointTerminalName("TerminalName");
		a1.setPostCode("code");
		a1.setPrefix("Pre");
		a1.setRegion(Calendar.getInstance().getTime());
		a1.setStreet("Street");
		a1.setTelephone("20012123");
		a1.setVatNumber("vat");
		
		Address a2=new Address();
		a2.setCity("Pune");
		a2.setCompany("Capgemini");
		a2.setCountryId("Ind");
		a2.setCustomerAddressLabel("CapgCorp");
		//a2.setEntityId(21L);
		a2.setFax("faxNo");
		a2.setFirstName("Anuja");
		a2.setIsDefaultBilling("Billing");
		a2.setIsDefaultShipping("Shiiping");
		a2.setLastName("khandelia");
		a2.setMissingVerification("missing");
		a2.setPickpointTerminalId("terminalId");
		a2.setPickpointTerminalName("TerminalName");
		a2.setPostCode("code");
		a2.setPrefix("Pre");
		a2.setRegion(Calendar.getInstance().getTime());
		a2.setStreet("Street");
		a2.setTelephone("20012123");
		a2.setVatNumber("vat");
		
		Address a3=new Address();
		a3.setCity("Pune");
		a3.setCompany("Capgemini");
		a3.setCountryId("Ind");
		a3.setCustomerAddressLabel("CapgCorp");
		//a2.setEntityId(21L);
		a3.setFax("faxNo");
		a3.setFirstName("Astha");
		a3.setIsDefaultBilling("Billing");
		a3.setIsDefaultShipping("Shiiping");
		a3.setLastName("khandelia");
		a3.setMissingVerification("missing");
		a3.setPickpointTerminalId("terminalId");
		a3.setPickpointTerminalName("TerminalName");
		a3.setPostCode("code");
		a3.setPrefix("Pre");
		a3.setRegion(Calendar.getInstance().getTime());
		a3.setStreet("Street");
		a3.setTelephone("20012123");
		a3.setVatNumber("vat");
		addressService.save(a3);
		addressService.save(a1);
		addressService.save(a2);
		List<Address> list=new ArrayList<>();
		list.add(a1);
		list.add(a2);
		
		List<Address> list1=new ArrayList<>();
		list1.add(a3);
		
		Communication com1=new Communication();
		com1.setCommunicationChannel("channel");
		com1.setCommunicationType("type");
		com1.setCountryCode("IND");
		com1.setCreationDate(new Timestamp(System.currentTimeMillis()));
		com1.setCustomerId(1L);
		com1.setEmail("amq@gmail.com");
		com1.setLanguageCode("lang");
		com1.setLeadId(44L);
		com1.setPermissionCaptureMethod("permission");
		com1.setPermissionDate(new Timestamp(System.currentTimeMillis()));
		com1.setPermissionStatus(2L);
		
		Communication com2=new Communication();
		com2.setCommunicationChannel("channel");
		com2.setCommunicationType("type");
		com2.setCountryCode("IND");
		com2.setCreationDate(new Timestamp(System.currentTimeMillis()));
		com2.setCustomerId(12L);
		com2.setEmail("amq@gmail.com");
		com2.setLanguageCode("lang");
		com2.setLeadId(47L);
		com2.setPermissionCaptureMethod("permission");
		com2.setPermissionDate(new Timestamp(System.currentTimeMillis()));
		com2.setPermissionStatus(29L);
		communicationService.save(com1);
		communicationService.save(com2);
		
		Customer c1 = new Customer();
		c1.setActive(true);
		c1.setCountryCode("12");
		c1.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		c1.setCreatedFrom("me");
		c1.setDataPrivacy(true);
		c1.setDniInf("dni");
		c1.setDob(Calendar.getInstance().getTime());
		c1.setEmail("abc@gmail.com");
		c1.setEmailVerified(true);
		c1.setEntityId(1L);
		c1.setExemptedFromPayingCopayment(true);
		c1.setFirstName("Pooja");
		c1.setFiscalCode("121");
		c1.setGhostAccountId(13);
		c1.setGroupId("we");
		c1.setInsuranceNumber("334");
		c1.setInvitedCustomerStatus("active");
		c1.setInvitedCustomerStatusDate(new Timestamp(System.currentTimeMillis()));
		c1.setLanguageCode("en");
		c1.setLastName("Jain");
		c1.setMeasurement("meas");
		c1.setNif("nif");
		c1.setNip("nip");
		c1.setOffLine(true);
		c1.setPayerInstitution(33);
		c1.setPaymentMethod("debit");
		c1.setPrefix("blue");
		c1.setPrepaymentChoice(44);
		c1.setPrivacyConsent(true);
		c1.setPrivacySurvey(true);
		c1.setSamplingOrderPlaced(true);
		c1.setSamplingProgramCode("program");
		c1.setSignatureCaputred(true);
		c1.setSubscribedForVatReduction(true);
		c1.setTaxVatNumber("vat");
		c1.setTermsAndConditionAgreed(true);
		c1.setUnder18(true);
		c1.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
		c1.setUpdatedSource("source");
		c1.setVatReductionEndDate(Calendar.getInstance().getTime());
		c1.setVatReductionStartDate(Calendar.getInstance().getTime());
		c1.setAddress(list);
		
		Customer c2 = new Customer();
		c2.setActive(true);
		c2.setCustomerLandline("numbwrr");
		c2.setCustomerMobile("opposite");
		c2.setCountryCode("13");
		c2.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		c2.setCreatedFrom("u");
		c2.setDataPrivacy(false);
		c2.setDniInf("dni");
		c2.setDob(Calendar.getInstance().getTime());
		c2.setEmail("bbc@gmail.com");
		c2.setEmailVerified(true);
		c2.setEntityId(24L);
		c2.setExemptedFromPayingCopayment(true);
		c2.setFirstName("Anuja");
		c2.setFiscalCode("121");
		c2.setGhostAccountId(13);
		c2.setGroupId("we");
		c2.setInsuranceNumber("334");
		c2.setInvitedCustomerStatus("active");
		c2.setInvitedCustomerStatusDate(new Timestamp(System.currentTimeMillis()));
		c2.setLanguageCode("en");
		c2.setLastName("Jain");
		c2.setMeasurement("meas");
		c2.setNif("nif");
		c2.setNip("nip");
		c2.setOffLine(true);
		c2.setPayerInstitution(33);
		c2.setPaymentMethod("debit");
		c2.setPrefix("blue");
		c2.setPrepaymentChoice(44);
		c2.setPrivacyConsent(false);
		c2.setPrivacySurvey(true);
		c2.setSamplingOrderPlaced(true);
		c2.setSamplingProgramCode("program");
		c2.setSignatureCaputred(true);
		c2.setSubscribedForVatReduction(true);
		c2.setTaxVatNumber("vat");
		c2.setTermsAndConditionAgreed(true);
		c2.setUnder18(true);
		c2.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
		c2.setUpdatedSource("source");
		c2.setVatReductionEndDate(Calendar.getInstance().getTime());
		c2.setVatReductionStartDate(Calendar.getInstance().getTime());
		c2.setAddress(list1);
		
		customerService.save(c1);
		customerService.save(c2);
		
	/*	List<Customer> customers = customerService.getAllCustomer();
		for (Customer customer : customers) {
			System.out.println(customer.getFirstName());
		}*/
		
		
	}

}
